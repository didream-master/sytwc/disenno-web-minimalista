# Diseño web minimalista
Dadas las páginas de los siguientes sitios web, indicar los elementos de diseño web minimalista que incorporan. Realizar un análisis de la usabilidad de los sitios:

* http://wmat.it/home
* http://madebytemple.com/
* http://paginasweb.staffcreativa.pe/?utm_source=blog&utm_medium=banner&utm_content=end-post&utm_campaign=Blog%20Banners


**Nota**

El los checklist de usabilidad realizados se han utilizado tres colores:
* Verde para señalar que cumple el requisito.
* Amarillo para señalar que el requisito no es aplicable al sitio web.
* Rojo para indicar que no cumple el requisito.


## Análisis de sitio web wmat
La primera impresión al entrar al sitio web no fue buena, puesto que no entiendo el propósito del sitio web al entrar. Además de ello, no funciona bien en dispositivos móviles y existe scroll en blanco, innecesario en páginas como https://wmat.it/whatsupintro. Aparte, en varias vistas de la página el video no se reproduce automáticamente puesto que no ha habido una interacción previa, quedando la vista en blanco.

En [CheckListNielsen1.pdf](CheckListNielsen1.pdf) se puede ver el checklist de usabilidad de Nielsen realizado sobre este sitio web dando como resultado que este sitio web, a pesar de tener un aspecto minimalista, es poco usable.

## Análisis de sitio web madebytemple
Al acceder al sitio web vemos que se trata de un blog con un diseño minimalista y una estructura clásica y familiar; con el menú de navegación en la parte superior que facilita la navegación por el sito web.

En [CheckListNielsen2.pdf](CheckListNielsen2.pdf) se puede ver el checklist de usabilidad realizado sobre este sitio y dejando de lado los requisitos no aplicables se puede decir que la usabilidad del sitio web es bastante buena, aunque mejorable.

## Análisis de sitio web staffcreativa
He podido realizar el checklist de usabilidad de este sitio web, sin embargo a día 30-06-2020 parece que ha dejado de funcionar dando el siguiente error:

```
WebFaction
Coming soon: Another fine website hosted by WebFaction.

Site not configured
...
```

El sitio web consiste en un sitio de una sola página, que cuenta con un menú de navegación con efecto scroll. Podemos ver que no dispone de un diseño minimalista, sin embargo aunque está cargado de contenido y animaciones, no quita que sea un diseño atractivo, desde mi punto de vista y usable, lo cual se refleja en el checklist de usabilidad realizado en [CheckListNielsen3.pdf](CheckListNielsen3.pdf).